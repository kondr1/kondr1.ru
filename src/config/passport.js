import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth'
import { Strategy as LocalStrategy } from 'passport-local'

// load up the user model
import User, { findById, findOne } from '../app/models/user'

// load the auth variables
import { googleAuth } from './auth'

export default function (passport) {
  passport.serializeUser((user, done) => done(null, user.id))

  // used to deserialize the user
  passport.deserializeUser((id, done) =>
    findById(id, (err, user) => done(err, user))
  )

  passport.use(
    new GoogleStrategy(
      {
        clientID: googleAuth.clientID,
        clientSecret: googleAuth.clientSecret,
        callbackURL: googleAuth.callbackURL
      },
      (token, refreshToken, profile, done) => {
        // make the code asynchronous
        // User.findOne won't fire until we have all our data back from Google
        process.nextTick(() =>
          // try to find the user based on their google id
          findOne({ 'google.id': profile.id }, (err, user) => {
            if (err) return done(err)

            if (user) {
              // if a user is found, log them in
              return done(null, user)
            } else {
              // if the user isnt in our database, create a new user
              var newUser = new User()

              // set all of the relevant information
              newUser.google.id = profile.id
              newUser.google.token = token
              newUser.google.name = profile.displayName
              newUser.google.email = profile.emails[0].value // pull the first email

              // save the user
              newUser.save(function (err) {
                if (err) throw err
                return done(null, newUser)
              })
            }
          })
        )
      }
    )
  )

  passport.use(
    'local-signup',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
      },
      (req, email, password, done) => {
        console.log(
          email +
            ' ' +
            password +
            ' SDASDASDASDASDASDASDASDASD!!!!!!!!!!!!!!!!!!!!!!!'
        )
        process.nextTick(() =>
          User.findOne({ 'local.email': email }, (err, user) => {
            if (err) return done(err)
            if (!user) {
              var newUser = new User()

              newUser.local.email = email
              newUser.local.password = newUser.generateHash(password)
              console.log(newUser)
              newUser.save(function (err) {
                if (err) throw err
                return done(null, newUser)
              })
            }
            return done(
              null,
              false,
              req.flash('signupMessage', 'That email is already taken.')
            )
          })
        )
      }
    )
  )

  passport.use(
    'local-login',
    new LocalStrategy(
      {
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
      },
      (req, email, password, done) =>
        User.findOne({ 'local.email': email }, (err, user) => {
          if (err) return done(err)
          if (!user) {
            return done(
              null,
              false,
              req.flash('loginMessage', 'No user found.')
            )
          } // req.flash is the way to set flashdata using connect-flash
          // if the user is found but the password is wrong
          if (!user.validPassword(password)) {
            return done(
              null,
              false,
              req.flash('loginMessage', 'Oops! Wrong password.')
            )
          } // create the loginMessage and save it to session as flashdata
          return done(null, user)
        })
    )
  )
}
