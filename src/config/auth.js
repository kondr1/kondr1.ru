import { env } from 'process'

export const host = env.HEROKU_APP_NAME
  ? `https://${env.APP_NAME}.herokuapp.com`
  : `http://kondr1.ru`

console.log(host)
console.log(env.APP_NAME)

export const googleAuth = {
  clientID: 'your-secret-clientID-here',
  clientSecret: 'your-client-secret-here',
  callbackURL: `${host}/auth/google/callback`
}

export default {
  googleAuth,
  host
}
