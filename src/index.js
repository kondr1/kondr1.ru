import express from 'express'
import passport from 'passport'
import flash from 'connect-flash'
import mongoose from 'mongoose'

import morgan from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import session from 'express-session'
import { url } from './config/database.js'
import pass from './config/passport'
import path from 'path'
import routes from './app/routes.js'

var app = express()
var port = process.env.PORT || 8080

mongoose.connect(url, { useNewUrlParser: true, debug: true })

app.use(morgan('dev'))
app.use(cookieParser())
app.use(bodyParser.urlencoded())
app.use(
  session({
    saveUninitialized: true,
    resave: true,
    secret: 'kek-lol-arbidol'
  })
)
app.use(passport.initialize())
app.use(passport.session())
app.use(flash())
app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'))

pass(passport)
routes(app, passport)

app.listen(port)
