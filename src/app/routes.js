const isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) return next()

  res.redirect('/')
}

export default (app, passport) => {
  app.get('/', (req, res) => {
    res.render('index.ejs')
  })

  app.get('/profile', isLoggedIn, (req, res) =>
    res.render('profile.ejs', {
      user: req.user
    })
  )

  app.get('/logout', (req, res) => {
    req.logout()
    res.redirect('/')
  })

  app.get('/login', (req, res) =>
    res.render('login.ejs', { message: req.flash('loginMessage') })
  )

  app.post(
    '/login',
    passport.authenticate('local-login', {
      successRedirect: '/profile',
      failureRedirect: '/login',
      failureFlash: true
    })
  )

  app.get('/signup', (req, res) =>
    res.render('signup.ejs', { message: req.flash('signupMessage') })
  )

  app.post('/signup', (req, res) => {
    console.log(passport)
    console.log('POST SIGNUP!!!!!~~~~~~~~')
    passport.authenticate('local-signup', {
      successRedirect: '/profile',
      failureRedirect: '/signup',
      failureFlash: true
    })(req, res)
  })

  app.get(
    '/auth/google',
    passport.authenticate('google', { scope: ['profile', 'email'] })
  )

  app.get(
    '/auth/google/callback',
    passport.authenticate('google', {
      successRedirect: '/profile',
      failureRedirect: '/'
    })
  )
}
