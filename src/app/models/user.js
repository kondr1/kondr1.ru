import mongoose from 'mongoose'
import { hashSync, genSaltSync, compareSync } from 'bcrypt-nodejs'

const userSchema = mongoose.Schema({
  local: {
    email: String,
    password: String
  },
  google: {
    id: String,
    token: String,
    email: String,
    name: String
  }
})

userSchema.methods.generateHash = password =>
  hashSync(password, genSaltSync(8), null)

userSchema.methods.validPassword = password =>
  compareSync(password, this.local.password)

export default mongoose.model('User', userSchema)
